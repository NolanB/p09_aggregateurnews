""" Projet Agrégateur de news : Groupe 1 """
############################################
""" Module by Alexis
- Création : 2020-03-8
- Dernière MàJ : 2020-03-16 by Alexis
"""
import DBProtocol, DBTab
import RSS_parse as rss
import psycopg2
from psycopg2 import Error
from werkzeug.security import generate_password_hash, check_password_hash #HLIB

def recuperer_news_flux(id_flux:int)->list:

    sql_id_news="select fK_Id_News from AssoFluxNews where FK_Id_Flux = %s"

    DBProtocol.cur.execute(sql_id_news,(id_flux,))
    liste_news= DBProtocol.cur.fetchall()
    return liste_news

def recuperation_id(sql_select:str,valeur_inserer:tuple)->int:

    DBProtocol.cur.execute(sql_select,valeur_inserer)

    try:
        id_table=DBProtocol.cur.fetchone()[0]
    except:
        id_table=None
    return id_table

def insert_new(dico_news:dict,id_flux:int,news_existe:bool)->None:
    if news_existe==False:
        DBProtocol.InsertTab(DBTab.InsTabNews,dico_news)
        id_news:int=DBProtocol.cur.fetchone()[0]
        DBProtocol.Commit()
    else:
        sql_news ="select PKId_News from TabNews where News_link = %s"
        id_news=recuperation_id(sql_news,(dico_news['News_link'],))

    dico_id:dict[int]={
        'PKId_Flux': id_flux,
        'PKId_News':id_news
        }
    sql_assoc:str="select PKId from AssoFluxNews where FK_Id_news = %s and FK_Id_Flux = %s"
    id_assox:int= recuperation_id(sql_assoc,(id_news,id_flux))
    if type(id_flux)==type(int()) and type(id_news)==type(int()):
        if type(id_assox)!=type(int()):
            DBProtocol.InsertTab(DBTab.InsAssoFluxNews,dico_id)
            DBProtocol.Commit()
    else:
        print('erreur id')

def insert_flux(dico_flux:dict,nom_flux:str= "default")->int:

    sql_flux ="select PKId_Flux from TabFluxs where Flux_url = %s"
    id_flux: int = recuperation_id(sql_flux,(dico_flux['url_flux'],))
    if type(id_flux)!=type(int()):
        dico_source={'Flux_url': str(dico_flux['url_flux']),'Flux_default_name': str(nom_flux)}
        DBProtocol.InsertTab(DBTab.InsTabFluxs,dico_source)
        id_flux: int=DBProtocol.cur.fetchone()[0]
        DBProtocol.Commit()

    else:
        print('le flux existe deja')

    return id_flux

def insert_news(news:dict,id_flux:int)->int:
    
    sql_news ="select PKId_News from TabNews where News_link = %s"
    id_news=recuperation_id(sql_news,(news['lien_news'],))
    dico_news={'News_title': news['titre'],'News_relase':news['date'],
                    'News_link': news['lien_news'],'News_body': news['description']}

    if type(id_news)!=type(int()) :
        try:
            insert_new(dico_news,id_flux,False)
        except:
            raise ValueError
    else:
        insert_new(dico_news,id_flux,True)
    return id_news

def insert_utilisateur(username:str , password:str,*,mail:str = None)->None:

    sql_user="select PKId_User from TabUsers where User_name = %s "
    id_user = recuperation_id(sql_user,(username,))
    hash = 'pbkdf2:sha256'
    password_hash = generate_password_hash(password, hash)

    if type(id_user)!=type(int()):

        dico:dict[str,bool]= {
            'User_Name': username,
            'User_pwd_hash': password_hash,
            'User_email': mail
        }
        try:
            DBProtocol.InsertTab(DBTab.InsTabUsers,dico)
            id_user=DBProtocol.cur.fetchone()[0]
        except Error :
            print('insert utilisateur echoué' + Error)
    else:
        print('Il y à déja un utilisateur')
    return id_user

def insert_toute_table(id_utilisateur:int,nom_flux:str,url_flux:int)->None:
    dico_flux= rss.recuperation_rss(url_flux)
    id_flux= insert_flux(dico_flux)
    for news in dico_flux['news']:
        insert_news(news,id_flux)
    # sql_flux ="select PKId_Flux from TabFluxs where Flux_url = %s"
    # id_flux: int = recuperation_id(sql_flux,(url_flux,))

    insert_asso_User_Flux(id_flux,id_utilisateur,nom_flux)

def insert_asso_User_Flux(id_flux:int,id_utilisateur:int,nom_flux:str)-> None:
    dico:dict[int,bool]={
        'FK_Id_User':id_utilisateur,
        'Flux_user_name':nom_flux,
        'Flux_display':True
        }

    sql_assoc="select PKId from AssoUserFlux where FK_Id_User = %s and FK_Id_Flux = %s"
    id_assox= recuperation_id(sql_assoc,(id_utilisateur,id_flux))

    ids_news=recuperer_news_flux(id_flux)
    if id_flux != None and id_utilisateur != None:

        if type(id_assox)== type(int()):

            try:
                sql_update=''' UPDATE AssoUserFlux set Flux_display=True where PKId = %s'''
                id_assox=str(id_assox)
                print(id_assox)
                DBProtocol.cur.execute(sql_update,(id_assox,))
                DBProtocol.Commit()

            except Error:
                print("l'update de assouserflux a rater" + Error)

        else:
            try:
                dico['FK_Id_Flux']=id_flux
                DBProtocol.InsertTab(DBTab.InsAssoUserFlux,dico)

            except:
                print("l'update de assouserflux a rater")

        for ids in ids_news:

            inserer_asso_user_news(ids[0],id_utilisateur)
            DBProtocol.Commit()
    else: 
        print('un id es null')

def inserer_asso_user_news(id_news:int,id_user:int)->None:

    sql_news ="select PKId from AssoUserNews where FK_Id_User = %s and FK_Id_News=%s"
    id_asso:int=recuperation_id(sql_news,(id_user,id_news))
    dico:dict[int,bool]={
        'FK_Id_User':id_user,
        'FK_Id_News':id_news,
        'News_display':False
        }
    if type(id_news)!= type(int) and type(id_user) != type(int) :
        if type(id_asso)!= type(int):
            try:
                DBProtocol.cur.execute(DBTab.InsAssoUserNews,dico)
            except Error :
                print(Error)
        else:
            sql_update: str=''' UPDATE AssoUserNews set news_display=True where fk_id_news = %s AND FK_Id_User = %s'''
            DBProtocol.cur.execute(sql_update,(id_news,id_user))
        DBProtocol.Commit()
    else:
        print("mauvais id")

def connexion_user(nom_user:str, password:str, inscription:bool=False)->bool :
    '''
    permet de verifier si un utilisateur existe ou pas
    '''
    sql_user = "select user_name from tabusers"
    DBProtocol.cur.execute(sql_user)
    list_user:list = DBProtocol.cur.fetchall()

    if inscription == False:
        sql_user = "select User_pwd_hash from tabusers where user_name = %s "
        DBProtocol.cur.execute(sql_user,[nom_user,])
        
        try :
            mdp_base:str = DBProtocol.cur.fetchone()[0]
            existe:bool =check_password_hash(mdp_base,password)
        except :
            existe:bool = False
        return existe, list_user
    else:
        return list_user
      
def recherche(mot_rechercher:str,id_user:int)->list:
    sql_news=f"""sELECT t.*,Af.Flux_user_name  
        FROM TabNews t,AssoUserNews Au,AssoUserFlux Af
        where
        t.PKId_News = Au.FK_Id_News
        AND t.PKId_News = Au.FK_Id_News
        AND Au.FK_Id_User = Af.FK_Id_User
        AND Au.News_display = FALSE
        and Af.FK_Id_User = {id_user}
        AND Af.Flux_display = TRUE
        and News_title like '%{mot_rechercher}%';"""
    #sql_article=f"select * from tabnews where News_title like '%{mot_rechercher}%' "
    DBProtocol.cur.execute(sql_news)
    liste_news=DBProtocol.cur.fetchall()
    return liste_news

def recherche_news_utilisateu(user_id:int)->list:
    sql_news="""sELECT t.*,Af.Flux_user_name  
        FROM TabNews t,AssoUserNews Au,AssoUserFlux Af
        where
        t.PKId_News = Au.FK_Id_News
        AND t.PKId_News = Au.FK_Id_News
        AND Au.FK_Id_User = Af.FK_Id_User
        AND Au.News_display = FALSE
        and Af.FK_Id_User = %s
        AND Af.Flux_display = TRUE;"""
    DBProtocol.cur.execute(sql_news,(user_id,))
    liste_news=DBProtocol.cur.fetchall()
    return liste_news

def supprimer_flux_utilisateur(id_flux:int,id_user:int)->None:
    sql_update_flux=''' UPDATE AssoUserFlux set Flux_display=False where FK_Id_User = %s and fk_id_flux = %s'''
    sql_update_news: str=''' UPDATE AssoUserNews set news_display=False where fk_id_news = %s AND FK_Id_User = %s'''
    DBProtocol.cur.execute(sql_update_flux,(id_user,id_flux))
    liste_news:list[int] =recuperer_news_flux(id_flux)
    for news in liste_news:
        try:
            DBProtocol.cur.execute(sql_update_news,(news,))
        except:
            print('erreur')
    
if __name__ == "__main__":
    DBProtocol.ConnexionBD()
    id_user=insert_utilisateur("bat","smash")
    url_rss=[
    "https://news.ycombinator.com/rss",
    "https://www.reddit.com/r/Python.rss",
    "https://hackernoon.com/feed",
    "https://www.usine-digitale.fr/rss",
    "http://www.joelonsoftware.com/rss",
    "https://code.facebook.com/posts/rss",
    "https://trello.engineering/feed.xml"
    ]

    for url in url_rss:
        insert_toute_table(id_user,"mo_flux",url_flux=url)
        existe, list_user = connexion_user('bat','smash')
    liste_news=recherche('python')
    for news in liste_news:
        print(news[1])

    DBProtocol.DeconnexionBD()