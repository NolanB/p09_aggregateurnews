import feedparser

def recuperation_rss(url_atom:str)->dict:
    '''
    url_atom: lien url du flux rss
    retourne un dictionnaire du format {url_flux: ,format: ,news: [{titre : , lien_news: , description : , date : },{titre : , lien_news: , description : , date : }, ... ]}
    '''
    liste_news=list()
    data = feedparser.parse(url_atom) # recupere le flux rss en format dictionnaire
    
    dico=dict()

    dico['url_flux']=data['feed']['subtitle_detail']['base']
    dico['format']=data['version'] #rss , atom

    #if dico['format']=='atom'
    dico['news']=list() # création d'une liste pour la clé news
    for dat in range(len(data['entries'])): #boucle sur le nombre de news du flux

        dico_news=dict() #reinitialisation du dictionnaire pour chaque news
        dico_news['titre']= data['entries'][dat]['title']
        dico_news['lien_news']= data.entries[dat].link
        #recupere la description de la news si elle existe sinon remplace par une valeur a recuperer
        try:
            dico_news['description']= data.entries[dat]['content'][0]['value']
        except:
            dico_news['description']= "a recuperer"
        
        try:
            dico_news['date']= data.entries[dat].published #date pour le rss
        except:
            dico_news['date']= data.entries[dat].updated #date pour atom
        dico['news'].append(dico_news) #rajoute le dico crée a la liste de la clé news 

    
    return dico

if __name__ == "__main__":
    url_rss=["https://news.ycombinator.com/rss","https://www.reddit.com/r/Python.rss",
    "https://hackernoon.com/feed","https://www.usine-digitale.fr/rss",
    "http://www.joelonsoftware.com/rss","https://code.facebook.com/posts/rss"]
    for url in url_rss:
        dico=recuperation_rss(url)
        print(dico['news'])
        print()
        
