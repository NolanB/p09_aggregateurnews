'''made by alexis '''
#import lxml.html
import feedparser
import re
import sys
import html.parser
     # html.parser in Python 3

def traitement_auteur(auteur:str):
    auteur.replace('/u/','')
    return auteur
def traitement_description(texte:str)->str:
    regex= '<[^>]+>'

    reg2='[comments]'   
    h = html.parser.HTMLParser()
    texte =h.unescape(texte)
    texte=re.sub(regex,' ',texte) 
    texte=texte.replace(reg2,'')
    texte=texte.replace('[link]','')
    # formats=chardet.detect(texte.encode('utf8'))
    # formats=formats['encoding']

    return texte

def creation_dico_news(data:dict,dat:int)-> dict:

    dico_news=dict() #reinitialisation du dictionnaire pour chaque news
    dico_news['titre']: str = data['entries'][dat]['title']
    dico_news['lien_news']: str= data.entries[dat].link
    #recupere la description de la news si elle existe sinon remplace par une valeur a recuperer
    try:
        dico_news['description']: str= traitement_description(data.entries[dat]['content'][0]['value'])

    except:
        value:str=traitement_description(data['entries'][dat]['summary_detail']['value'])
        #if value.startswith('<a href=')==True:
        if value=='':
            dico_news['description']: str = "inconnu"
        else:
            dico_news['description']: str = value


    try:
        dico_news['auteur'] : str = traitement_auteur(data.entries[dat]['author'])
    except:
        dico_news['auteur'] : str = 'inconnu'

    try:
        dico_news['date']= data.entries[dat].published #date pour le rss

    except:
        dico_news['date']= data.entries[dat].updated #date pour atom

    return dico_news

def recuperation_rss(url_rss:str)->dict:
    '''
    url_atom: lien url du flux rss
    retourne un dictionnaire du format {url_flux: ,format: ,news: [{titre : , lien_news: , description : , date : },{titre : , lien_news: , description : , date : }, ... ]}
    '''

    try:
        #data = feedparser.parse.rdf(feed.read(url_rss))

        data = feedparser.parse(url_rss) # recupere le flux rss en format dictionnaire    
    except:
        raise KeyError
    dico=dict()
    dico['url_flux'] : str = url_rss
    dico['format'] : str = data['version'] #rss , atom
    dico['news'] : list[dict] = list() # création d'une liste pour la clé news
    for dat in range(len(data['entries'])): #boucle sur le nombre de news du flux

        dico['news'].append(creation_dico_news(data,dat)) #rajoute le dico crée a la liste de la clé news 

    return dico

if __name__ == "__main__":

    url_rss=[
    "https://news.ycombinator.com/rss",
    "https://www.reddit.com/r/Python.rss",
    "https://hackernoon.com/feed",
    "https://www.usine-digitale.fr/rss",
    "http://www.joelonsoftware.com/rss",
    "https://code.facebook.com/posts/rss",
    "https://trello.engineering/feed.xml"
    ]

    for url in url_rss:
        dico=recuperation_rss(url)
        for news in dico['news']:
            print(news['description'])