""" Projet Agrégateur de news : Groupe 1 """
############################################
""" script by Julien
- Création : 2020-03-06
- Dernière MàJ : 2020-03-12
"""

################ Tables étape 1 ################

TabFluxs = """
  CREATE TABLE TabFluxs (
    PKId_Flux SERIAL PRIMARY KEY,
    Flux_url TEXT NOT NULL,
    Flux_default_name TEXT NOT NULL
  )"""

InsTabFluxs = """
  INSERT INTO TabFluxs (Flux_url, Flux_default_name)
  VALUES (%(Flux_url)s, %(Flux_default_name)s) returning PKId_Flux;"""


TabNews = """
  CREATE TABLE TabNews (
    PKId_News BIGSERIAL PRIMARY KEY,
    News_title TEXT NOT NULL,
    News_relase TIMESTAMP,
    News_link TEXT NOT NULL,
    News_body TEXT
  )"""

InsTabNews = """
  INSERT INTO TabNews (News_title, News_relase, News_link, News_body)
  VALUES (%(News_title)s, %(News_relase)s, %(News_link)s, %(News_body)s) returning PKId_News;"""

################ Table d'association étape 1 ################

AssoFluxNews = """
  CREATE TABLE AssoFluxNews (
    PKId BIGSERIAL PRIMARY KEY,
    FK_Id_Flux INT,
    FK_Id_News INT,
    FOREIGN KEY(FK_Id_Flux) REFERENCES TabFluxs(PKId_Flux),
    FOREIGN KEY(FK_Id_News) REFERENCES TabNews(PKId_News)
  )"""

InsAssoFluxNews = """
  INSERT INTO AssoFluxNews (FK_Id_Flux, FK_Id_News)
  VALUES (%(PKId_Flux)s, %(PKId_News)s)
  ;"""

################ Tables étape 2 ################

TabUsers = """
  CREATE TABLE TabUsers (
    PKId_User BIGSERIAL PRIMARY KEY,
    User_Name TEXT NOT NULL,
    User_pwd_hash TEXT,
    User_email TEXT
  )"""

InsTabUsers = """
  INSERT INTO TabUsers (User_Name, User_pwd_hash, User_email)
  VALUES (%(User_Name)s, %(User_pwd_hash)s, %(User_email)s) returning PKid_user
  ;"""

################ Table d'association étape 2 ################

AssoUserNews = """
  CREATE TABLE AssoUserNews (
    PKId BIGSERIAL PRIMARY KEY,
    FK_Id_User INT,
    FK_Id_News INT,
    News_display BOOLEAN default FALSE,
    FOREIGN KEY(FK_Id_User) REFERENCES TabUsers(PKId_User),
    FOREIGN KEY(FK_Id_News) REFERENCES TabNews(PKId_News)
  )"""

InsAssoUserNews = """
  INSERT INTO AssoUserNews (FK_Id_User, FK_Id_News, News_display)
  VALUES (%(FK_Id_User)s, %(FK_Id_News)s, %(News_display)s)
  ;"""


AssoUserFlux = """
  CREATE TABLE AssoUserFlux (
    PKId BIGSERIAL PRIMARY KEY,
    FK_Id_User INT,
    FK_Id_Flux INT,
    Flux_user_name TEXT,
    Flux_display BOOLEAN default TRUE,
    FOREIGN KEY(FK_Id_User) REFERENCES TabUsers(PKId_User),
    FOREIGN KEY(FK_Id_Flux) REFERENCES TabFluxs(PKId_Flux)
  )"""

InsAssoUserFlux = """
  INSERT INTO AssoUserFlux (FK_Id_User, FK_Id_Flux, Flux_user_name, Flux_display)
  VALUES (%(FK_Id_User)s, %(FK_Id_Flux)s, %(Flux_user_name)s, %(Flux_display)s) 
  ;"""

################ Tables étape 3 ################

## next week, a new episode...